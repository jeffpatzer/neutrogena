/*
  Mappings

  Mappings are matchers that we use to determine if we should execute a
  bit of Tritium during an execution. Aka, run something when we are
  are on a certain page.

  Example starting code:
*/

match($status) {

  with(/302/) {
    log("--> STATUS: 302") # redirect: just let it go through
  }

  with(/200/) {
    log("--> STATUS: 200")

    match($path) {
      with(/^\/$|^\/\?/) {
        log("--> Importing pages/home.ts in mappings.ts")
        @import "pages/home.ts"
      }
      with(/(product|produits)\/\w+$/) {
        log("--> Importing pages/product.ts in mappings.ts")
        @import "pages/product.ts"
      }
      with(/(product|produits)\/\w+/) {
        log("--> Importing pages/detail.ts in mappings.ts")
        @import "pages/detail.ts"
      }
      with(/legal-statement|privacypolicy|cookie-policy|contactus|sitemap/) {
        log("--> Importing pages/footer_links.ts")
        @import "pages/footer_links.ts"
      }
      else() {
        log("--> No page match in mappings.ts")
      }
    }
  }

  else() {
    # not 200 or 302 response status
    log("--> STATUS: " + $status + "; assuming it's an error code and importing pages/error.ts")
    @import "pages/error.ts"
  }

}
