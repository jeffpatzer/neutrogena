$('./body') {

  # Removals
  $(".//div[contains(@class, 'pane-product-catalog')]") {
    remove()
  }

  $(".//div[@class='header']") {
    add_class("mw_header")
    remove(".//img[@class='print_func_show']")
    remove("//div[contains(@class, 'pane-menu-menu-mobile-menu')]")

    $(".//div[contains(concat(' ', @class, ' '), ' logo ')]") {
      move_to("../..", "top")
    }


    $(".//div[@class='primary_navigation']") {
      $("./ancestor::div[@class='menuDeskTab']") {
        insert_before("div", class: "sprites-menu")
        insert_before("div", class: "mw_search sprites-search")
      }

      $(".//a[contains(@href, 'javascript')]") {
        name("div")
        remove("@href")
      }

      $("./ul") {
        $("./li") {
          move_here("//div[@class='slidemeup_inner_wrap']/ul[1]", "bottom") {
            remove("@style")
            remove(".//li[@class='pipe']")
          }
          # ur_set("toggler")
          ur_toggler("./div", "./ul")
          $("./div|./a") {
            add_class("mw_bar_black")
          }
          $("./ul/li") {
            add_class("mw_bar_gray4")
          }
        }
      }
    }

    $(".//div[@class='logo_primari_list_search_contaoner']") {
      $("./div[contains(concat(' ', @class, ' '), ' search_box ')]") {
        $("./form//div[@class='form-item']") {
          add_class("mw_bar_black")
          $("./input") {
            attributes(value: "", placeholder: "Search")
          }
        }
      }
      remove("./div[contains(@class, 'mobile')]")
      ur_tabs("./div[@class='sprites-menu']|./div[contains(@class,'sprites-search')]", "./div/div/ul|./div/form")
      # ur_tabs("./div[contains(@class,'sprites-search')]", "./div/form")
      ur_attribute("closeable", "true")
      $("./div[@class='sprites-menu']") {
        attribute("data-ur-state", "disabled")
      }
      $(".//div[@class='primary_navigation']") {
        $("./ul") {
          attribute("data-ur-state", "disabled")
        }
      }
      $("./div[contains(@class, 'search_box')]") {
        attribute("data-ur-state", "disabled")
      }
    }
  }

  $(".//div[@class='breadcrumb']") {
    add_class("breadcrumbs")
    $("./span") {
      name("li")
      $("./span") {
        name("a")
      }
    }
    $("./span[position()=last()]") {
      add_class("unavailable")
    }
  }
}
