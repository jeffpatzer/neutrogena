$("./body") {
  add_class("mw_detail")
  remove(".//div[contains(concat(' ', @class, ' '), ' panels-flexible-row-ProductFamilyHome-1 ')]")
  remove(".//div[@class='block_product_detail block_product_detail_row2']")

  $(".//h2[@class='pane-title']") {
    add_class("mw_h1")
  }

  $(".//div[contains(@class, 'panels-flexible-row-ProductFamilyHome')]") {

  }

  $(".//div[@id='prod_bg_img_cont_holder']") {
    $("./img") {
      remove("@width")
      remove("@height")
    }
  }

  $(".//div[@class='product_catalog_tabs']") {
    $("./div") {
      %id = fetch("@id")
      %move_string = "../ul/li/a[@href='#" + %id + "']"
      log(%move_string)
      move_to(%move_string, "after")
    }
    $("./ul") {
      $("./li") {
        ur_toggler("./a", "./div")
        $("./a") {
          name("div")
          add_class("mw_bar_black")
        }
      }
    }
  }

  $(".//div[@class='products_recommends']") {
    $("./h3") {
      add_class("mw_h2")
    }
    $(".//div[@class='product_blocks']") {
      add_class("row")
      $("./div[@class='product_display']") {
        insert_after("div", class: "small-8 columns mw_items_container") {
          move_here("../*[not(@class='product_display')]", "bottom")
        }
        add_class("small-4 columns")
      }
    }
  }
}
