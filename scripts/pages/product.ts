$("./body") {
  add_class("mw_product")

  $(".//div[@class='breadcrumb']") {
    add_class("breadcrumbs")
    $("./span") {
      name("li")
      $("./span") {
        name("a")
      }
    }
    $("./span[position()=last()]") {
      add_class("unavailable")
    }
  }

  $(".//div[@class='Family_Heading']") {
    $("./h4") {
      add_class("mw_page_heading")
      add_class("mw_bar_black")
    }
  }

  $(".//div[@class='product_cat_wrapper']") {
    $("./div[@class='cat_pro_list']") {
      $(".//div[contains(concat(' ', @class, ' '), ' ind_prod ')]") {
        add_class("row")
        insert_top("div", class: "small-8 columns mw_second_column")
        insert_top("div", class: "small-4 columns mw_img_column") {
          move_here("..//img", "bottom")
        }
        $("./div[contains(concat(' ', @class, ' '), ' small-8 ')]") {
          $("./self::following-sibiling") {
            add_class("mw_testing")
          }
          move_here("../*[not(contains(@class,'columns'))]", "bottom")
        }
      }
    }
  }
}
