$("./body") {
  add_class("mw_home")

  # remove(".//div[contains(@class, 'ntg_home_page_wrapper_canvas_carousel')]")
  remove(".//div[@id='content']/ul/li[position()<last()]")
  remove(".//div[@id='content']/span")
  remove(".//div[contains(@class, 'panels-flexible-region-ntg_home_page_wrapper_canvas-regoin_1')]")

  $(".//div[contains(concat(' ', @class, ' '), ' homeContent-ntg_home_page_wrapper_canvas-7-inside ')]") {
    $("./div") {
      add_class("mw_row")
    }
    $(".//div[contains(concat(' ', @class, ' '), ' panel-pane ')]") {
      add_class("row")
      $("./div/div/div") {
        add_class("row")
        $("./a") {
          add_class("small-5")
          add_class("columns")
          add_class("mw_small_img")
        }
        $("./div") {
          add_class("small-7")
          add_class("columns")
        }
      }
    }
  }

  $(".//div[@id='content']") {
    insert_bottom("div", class: "mw-dots") {
      insert_bottom("div", class: "mw-dot")
      insert_bottom("div", class: "mw-dot")
      insert_bottom("div", class: "mw-dot")
      insert_bottom("div", class: "mw-dot")
    }
  }

  # $(".//div[contains(concat(' ', @class, ' '), ' column2 ')]") {
  #   remove_class("column2")
  #   add_class("small-6 columns")
    # $("..") {
    #   add_class("row")
    #   add_class("mw_pics")
    # }
    # $(".//div[contains(concat(' ', @class, ' '), ' panel-pane ')]/div/div") {
    #   $("./div") {
    #     add_class("row")
    #     $("./a") {
    #       add_class("small-5")
    #       add_class("columns")
    #     }
    #     $("./div") {
    #       add_class("small-7")
    #       add_class("columns")
    #     }
    #   }
    # }
  # }

  # $(".//div[contains(concat(' ', @class, ' '), ' column3 ')]") {
  #   remove_class("column3")
  #   add_class("small-6 columns")
  #   $(".//div[contains(concat(' ', @class, ' '), ' panel-pane ')]/div/div") {
  #     $("./div") {
  #       add_class("row")
  #       $("./a") {
  #         add_class("small-5")
  #         add_class("columns")
  #       }
  #       $("./div") {
  #         add_class("small-7")
  #         add_class("columns")
  #       }
  #     }
  #   }
  # }

  $(".//div[contains(concat(' ', @class, ' '), ' panel-flexible-inside ')]") {
    insert_bottom("div", class: "mw_home_page_nav") {
      copy_here("//div[@class='primary_navigation']/ul/li", "bottom")
      $("./li") {
        name("div")
        remove("@data-ur-toggler-component")
      }
    }
  }
}
